# FlySilico 1.0 - A Drosophila melanogaster metabolic network. 
###### Authors: Lisa Jehrke[1], Tabea Mettler-Altmann[2], Juergen Schoenborn[1] and Mathias Beller[1]

[1]\
Institute for Mathematical Modeling of Biological Systems\
Systems Biology of Lipid Metabolism\
Heinrich Heine University, Duesseldorf, Germany
    
[2]\
Institute of Plant Biochemistry & Cluster of Excellence on Plant Sciences\
Heinrich Heine University, Duesseldorf, Germany
    
https://doi.org/10.1038/s41598-019-53532-4

#### Repository of FlySilico 1.0

- Environment creation is covered by readme1p1.html
- **change branch for Python 3.7 environment package**

